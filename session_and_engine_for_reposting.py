from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy import event
engine = create_engine('sqlite:///C:\\Users\\Vladimir\\Desktop\\ПИТОНЬИ задания\\piterbot\\Data _Base\\Data.db', echo=True)
z_session = scoped_session(sessionmaker(autocommit=False, autoflush=True, bind=engine))

@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.close()
