#Данный модуль содержит Класс Process, а так же алгоритм функций, обеспечивающий возможность пользователю создавать и сохранять свои Объявления.
from messages import * # Инмпортируем все с файла сообщений
from telebot import types
from bot import bot # Импортируем объект бота


processes = dict()

class Process: #данный класс содержит объекты конструктора посредством которых пользователь создает свои объявления
    def __init__(self, telegramId, finishedCallback):
        global processes
        self._telegram_id = telegramId
        self._callback = finishedCallback
        self._text_post = ''
        self._price = ''
        self._field_of_activity = ''

        #processes[telegramId] = self;

    
    def post_start(self, message): # начало регистрации объявления
        bot.send_message(self._telegram_id, POST_START_MESSAGE)
        self.show_text_post(message)


    def show_text_post(self, message): # позволяет пользователю ввести текст своего объявления 
        bot.send_message(self._telegram_id, POST_TEXT_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_text_post(message) )

    def get_text_post(self, message):# бот примнимает текст, объявления, при этом проводится проверка на то, что бы пользователь не ввел иную команду из модуля Хэндлер 
        self._text_post = message.text
        if message.text[0]=='/':
            self.show_text_post(message)
            return
        self.show_price(message)

    def show_price(self, message): #позволяет пользователю ввести интересуюемую его стоимоть
        bot.send_message(self._telegram_id, POST_PRICE_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_price(message) )

    def get_price(self, message): #бот примнимает текст цены, при этом проводится проверка на то, что бы пользователь не ввел иную команду из модуля Хэндлер
        self._price = message.text
        if self._price[0]=='/':
            self.show_price(message)
            return
        self.show_field_of_activity(message)

    def show_field_of_activity(self, message): #позволяет пользователю ввести интересуюемую его сферу деятельности
        bot.send_message(self._telegram_id, POST_FIELD_OF_ACTIVITY_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_field_of_activity(message) )

    def get_field_of_activity(self, message): #бот примнимает текст сферы деятельснои, при этом проводится проверка на то, что бы пользователь не ввел иную команду из модуля Хэндлер
        self._field_of_activity = message.text
        if self._field_of_activity[0]=='/':
            self.show_field_of_activity(message)
            return
        bot.send_message(self._telegram_id, POSTREG_THANKS_MESSAGE)
        if self._callback is not None:
            self._callback(self)



    def text_post(self): # позволяет возвращать ввденные пользователм значения переменных
        return self._text_post

    def price(self):
        return self._price

    def field_of_activity(self):
        return self._field_of_activity

    def telegram_id(self):
        return self._telegram_id

            
            
        
        
    
        
