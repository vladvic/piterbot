from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy import event
engine = create_engine('mysql+pymysql://root:rootpass@localhost/cotonti')
q_session = scoped_session(sessionmaker(autocommit=False, autoflush=True, bind=engine))

@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    #cursor.execute("PRAGMA foreign_keys=ON")
   # cursor.execute("PRAGMA case_sensitive_like=OFF")
    cursor.close()
