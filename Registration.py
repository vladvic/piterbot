# Данный модуль позволят осуществить процесс регистраиции пользователя(аккаунта) в боте  и имеет, в большинстве своем, схожий функционал с модулем Post_Data
from messages import * # Инмпортируем все с файла сообщений
from telebot import types
from bot import bot # Импортируем объект бота



processes = dict()

class Process:
    def __init__(self, telegramId, finishedCallback):
        global processes
        self._telegram_id = telegramId
        self._callback = finishedCallback
        self._city = ''
        self._country = ''
        self._location_latitude = ''
        self._location_longitude = ''
        self._type = ''
        self._phone = ''
        self._email = ''
        self._name = ''

        #processes[telegramId] = self;

    def start(self, message):
        bot.send_message(self._telegram_id, REG_NAME_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_name(message) )
        
    def get_name(self, message):
        self._name = message.text
        if self._name[0]=='/':
            self.start(message)
            return
        self.show_phone(message)

    def show_phone(self, message):
        keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
        button_phone = types.KeyboardButton(text=REG_SEND_PHONE_MESSAGE, request_contact=True)
        keyboard.add(button_phone)
        bot.send_message(self._telegram_id, REG_PHONE_MESSAGE, reply_markup=keyboard)
        bot.register_next_step_handler(message, lambda message: self.get_phone(message) )

    def get_phone(self, message):
        if message.contact is None:
            self.show_phone(message)
            return
        self._phone = message.contact.phone_number
        self.show_email(message)

    def show_email(self, message):
        bot.send_message(self._telegram_id, REG_EMAIL_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_email(message) )

    def get_email(self, message):
        if message.text is None:
            self.show_email(message)
            return
        self._email = message.text
        if self._email[0]=='/':
            self.show_email(message)
            return
        self.show_type(message)

    def show_type(self, message): # Позволяет создавать меню кнопок для выбора вариантов регистарции пользователя в качесвте соискателя или рекрутера. 
        keyboard = types.InlineKeyboardMarkup()
        button_offer = types.InlineKeyboardButton(REG_SEND_OFFER, callback_data="type offer")
        button_need = types.InlineKeyboardButton(REG_SEND_NEED, callback_data="type need")
        keyboard.add(button_offer, button_need)
        bot.send_message(self._telegram_id, REG_TYPE_MESSAGE, reply_markup=keyboard)
        

        
        @bot.callback_query_handler(func=lambda call: (call.data[0:5] == "type " and call.message.chat.id == self._telegram_id and self._type == ''))
        def process_type(call):
            
            if call.data == 'type offer':
                self._type = 'offer'
            elif call.data == 'type need':
                self._type = 'need'
            else:
                bot.send_message(self._telegram_id, REG_WRONG_TYPE)
                self.show_type(call.message)
                return
            self.show_country(call.message)

    def show_country(self, message):
        bot.send_message(self._telegram_id, REG_COUNTRY_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_country(message) )

    def get_country(self, message):
        self._country = message.text
        if self._country[0]=='/':
            self.show_country(message)
            return
        self.show_city(message)

    def show_city(self, message):
        bot.send_message(self._telegram_id, REG_CITY_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_city(message) )

    def get_city(self, message):
        self._city = message.text
        if self._city[0]=='/':
            self.show_city(message)
            return
        self.show_location(message)

    def show_location(self, message): # позвляет фиксацию данных текущей геопозиции пользователя для регистрации в базе данных
        keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
        button_location = types.KeyboardButton(text=REG_SEND_LOCATION_MESSAGE, request_location=True)
        keyboard.add(button_location)
        bot.send_message(self._telegram_id, REG_LOCATION_MESSAGE, reply_markup=keyboard)
        bot.register_next_step_handler(message, lambda message: self.get_location(message) )

    #@bot.message_handler(content_types=["location"])
    def get_location(self, message):
        self._location_latitude = message.location.latitude
        self._location_longitude = message.location.longitude
        bot.send_message(self._telegram_id, REG_THANKS_MESSAGE)
        if self._callback is not None:
            self._callback(self)

    def name(self):
        return self._name

    def phone(self):
        return self._phone

    def email(self):
        return self._email

    def type(self):
        return self._type

    def location_lati(self):
        return self._location_latitude 
    
    def location_longi(self):
        return self._location_longitude

    def country(self):
        return self._country

    def city(self):
        return self._city

    def telegram_id(self):
        return self._telegram_id

