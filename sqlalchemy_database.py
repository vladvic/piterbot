# Данный модуль содержит два Класса Post, User 
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
from sqlalchemy import Column, Integer, String, ForeignKey, Float, BigInteger
from sqlalchemy.orm import relationship
from sqlalchemy import Sequence
from session_sqlalchemy_database import session, engine
from sqlalchemy import MetaData
meta = MetaData()
from sqlalchemy.orm import object_mapper


#from sqlalchemy.orm import sessionmaker
#Session = sessionmaker(bind=engine)
#Session.configure(bind=engine)# как только двигатель будет доступен
#session = Session()#Этот пользовательский класс сеанса создаст новые объекты сеанса,
#которые будут привязаны к нашей базе данных.
#Другие транзакционные характеристики также могут
#быть определены при вызове sessionmaker;
#Всякий раз,когда вам нужно
#поговорить с базой данных, вы создаете указаный выше экземпляр сеанса

class Post(Base):
    __tablename__ = 'posts'

    id = Column(Integer, Sequence('post_id_seq'), primary_key=True)
    text_post = Column(String)
    text_post_lower = Column(String)
    price = Column(BigInteger)
    field_of_activity = Column(String)
    field_of_activity_lower = Column(String)
    users_id = Column(Integer, ForeignKey('users.id')) # "Дружба" между объектами классов Post и User, и взаимосвязь соответствующих таблиц

    def __repr__(self):
        return "<Post(text_post='%s', text_post_lower='%s' price='%s', field_of_activity='%s', field_of_activity_lower='%s'>)" % (self.text_post, self.text_post_lower, self.price, self.field_of_activity, self.field_of_activity_lower)


class Cot_Post(Base):
    __tablename__ = 'table_cot'

    id = Column(Integer, Sequence('table_cot_id_seq'), primary_key=True)
    item_title = Column(String)
    item_text = Column(String)

    def __repr__(self):
        return "<Cot_Post(item_title='%s', item_text='%s')" % (self.item_title, self.item_text)    


class User(Base):
     __tablename__ = 'users'
     
     id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
     phone = Column(String)
     name = Column(String)
     email = Column(String)
     country = Column(String)
     city = Column(String)
     location_latitude = Column(Float)
     location_longitude = Column (Float)
     telegram_id = Column(String, unique=True)
     post = relationship("Post", backref="user") # "Дружба" между объектами классов Post и User, и взаимосвязь соответствующих таблиц
     
     

     def __repr__(self):
         return "<User(phone='%s', name='%s', email='%s', country='%s', city='%s', location_latitude='%s', location_longitude='%s')>" % (self.phone, self.name, self.email, self.country, self.city, self.location_latitude, self.location_longitude)

Base.metadata.create_all(engine)
if __name__=='__main__':
     vl_user = User(phone='89234313376', name='Иванов Владимир', email='ivc86@yandex.ru', country='РФ', city='Томск', location_latitude='0', location_longitude='1')
     
     session.add(vl_user)
     our_user = session.query(User).filter_by(name='Иванов Владимир').first()
     
     




    





                                  
