import sqlite3
import os


name_db = 'database.db'
cur_dir = os.getcwd()
path_db = os.path.join(cur_dir,name_db)

#проверка на существование бызы данных
if not os.path.exists(path_db):
        try:
                #создание базы данных + создание таблиц
                conn = sqlite3.connect(path_db)
                # если не сработает, то sudo chmod 777 ./database.db
                # os.popen('sudo chmod 777 ' + path_db).read()
                cursor = conn.cursor()


                #создание таблиц + наполнение
                cursor.executescript("""BEGIN TRANSACTION;
			CREATE TABLE "user" (
				`id`    INTEGER PRIMARY KEY AUTOINCREMENT,
				`name`    TEXT,
				`id_lang`    INTEGER,
				`changed`    TEXT
			);

			INSERT INTO `user`  (name, id_lang, changed)  
			VALUES('Егор',1, DATETIME('now'));
			INSERT INTO `user`  (name, id_lang, changed)  
			VALUES('Иван',2, DATETIME('now'));

			CREATE TABLE "lang" (
				`id`    INTEGER PRIMARY KEY AUTOINCREMENT,
				`name`    TEXT,
				`changed`    TEXT
			);

			INSERT INTO `lang`  (name, changed)  
			VALUES('python',DATETIME('now'));
			INSERT INTO `lang`  (name, changed)  
			VALUES('ruby',DATETIME('now'));

			COMMIT;
		""")
               # фиксирую коммит
                conn.commit()
            
        except sqlite3.Error as e:            
                print('Ошибка БД: ' + str(e))
            
