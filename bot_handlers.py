#Данный модуль определяет рабочие инструменты бота и его основные команды.
#Это, своего рода "руки" нашего телеграмбота.
from bot import bot # Импортируем объект бота.
from messages import * # Инмпортируем все с файла сообщений.
from telebot import types #Импортируем возможност использоват types.
from emoji import emojize #Импортируем эмодзи.
import Registration #Позволяет использоват уже созданынй модуль Registration, который обеспечивает процесс регистрации пользователя. 
import Post_Data #Позволяет импортировать уже созданный модуль Post_Data, который обеспечивает процесс регистрации пользователем своих объявлений. 
from sqlalchemy_database import User, Post #Позволяет импортировать из уже созданного модуля sqlalchemy_database классы User, Post.
from session_sqlalchemy_database import session #Позволяет импортировать из уже созданного модуля session_sqlalchemy_database объект(переенную)session. 
import search_process_data #Импортирует уже созданный модуль search_process_data, необходимый для функционирования /search.
import re #Позволяет позволяет использовать язык регулярных вырожений. 
from messages import *#Позволет использовать сообщения из созданного модуля message.
from sqlalchemy import update #Позволяет использовать метод update для обновления данных геолокации.




#переменные в каторых храним ответы
name = ''; # ФИО
service_description=''; #Тип услуги или товара
service_price=''; # Цена
service_town=''
service_country=''
service_email=''




@bot.message_handler(commands=['start']) # Выполняется, когда пользователь нажимает на start
def send_welcome(message):
    bot.send_message(message.chat.id, HELLO_MESSAGE)

    
@bot.message_handler(commands=['help']) # Выполняется, когда пользователь /help
def send_welcome(message):
    bot.send_message(message.chat.id, HELLO_MESSAGE)

@bot.message_handler(commands=["menu"])# Данная команда на данный момент не доступна.
def geophone(message):
    # Эти параметры для клавиатуры необязательны, просто для удобства
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    button_phone = types.KeyboardButton(text="Отправить номер телефона", request_contact=True)
    button_geo = types.KeyboardButton(text="Отправить местоположение", request_location=True)
    button_price = types.KeyboardButton(text="Запросить цены", request_location=True)
    button_rules = types.KeyboardButton(text="Правила", request_location=True)
    button_humor = types.KeyboardButton(text="Юмор дня", request_location=True)
    keyboard.add(button_phone, button_geo, button_price, button_rules, button_humor )
    bot.send_message(message.chat.id, "Сообщите, пожалуйста, свой номер телефона или поделитесь местоположением. Спасибо!", reply_markup=keyboard)




def reg_finished(process):  # Заверашет процесс регистрации пользователя с фиксацией его в таблице базы данных User
    print(process.__dict__)
    
    vl_user = User(phone=process.phone(), name=process.name(), email=process.email(), country=process.country(), city=process.city(),  location_latitude= str(process.location_lati()), location_longitude= str(process.location_longi()), telegram_id=process.telegram_id())
    if find_user(process.telegram_id()) is  None:
        session.add(vl_user)
        session.commit()
    else:
        bot.send_message(process.telegram_id(), STOP_REG)
        return

    
    

@bot.message_handler(commands=['reg']) # Выполняется, когда пользователь /reg, то еть регситрации
def register(message):
    if find_user(message.chat.id) is not None:
       bot.send_message(message.chat.id, STOP_REG)
       return
    reg = Registration.Process(message.chat.id, reg_finished)
    reg.start(message)

def post_reg_finished(process): # Заверашет процесс регистрации объявлений пользователя с фиксацией его таблице в базы данных Post
    print(process.__dict__)
    user = find_user(process.telegram_id())
    vl_post = Post(text_post=process.text_post(), text_post_lower=process.text_post().lower(), price=re.sub('[\D]+', "", process.price()), field_of_activity=process.field_of_activity(), field_of_activity_lower=process.field_of_activity().lower(), user=user)
    if find_user(process.telegram_id()) is  None:
        bot.send_message(process.telegram_id(), STOP_POST)
        
    else:
        session.add(vl_post)
        session.commit()
        return


@bot.message_handler(commands=['post']) # Выполняется, когда пользователь /post, то есть регистрация объявлений.
def post_register(message):
    if find_user(message.chat.id) is None:
        bot.send_message(message.chat.id, STOP_POST)
        return
    post=Post_Data.Process(message.chat.id, post_reg_finished)
    post.post_start(message)

def find_user(telegram_id):
    try:
        user=session.query(User).filter(User.telegram_id == telegram_id).one()
        return user
    except:
        return None

@bot.message_handler(commands=['search']) # Выполняется, когда пользователь /search
def post_sercher(message):
    if find_user(message.chat.id) is None:
        bot.send_message(message.chat.id, STOP_POST)
        return
    post_search=search_process_data.Post_search_process(message.chat.id, fin_call)
    post_search.post_search_start(message)

def fin_call(process):
    print(process.__dict__)


@bot.message_handler(commands=['my_posts']) # Выполняется, когда пользователь /my_posts, для просмотра полного списка собственных объявлений пользователя
def full_posts_list(message):
    user = find_user(message.chat.id)
    if find_user(message.chat.id) is None:
        bot.send_message(message.chat.id, YOU_NOT_REG_MES)
        return
    posts_lists = list(user.post)
    if not posts_lists:
        bot.send_message(message.chat.id, YOU_HAVE_NOT_POST_MES)
        return  
    for post_1 in posts_lists: 
        keyboard = types.InlineKeyboardMarkup()
        button_del = types.InlineKeyboardButton(DEL_POST_BUTTON, callback_data="del_post {}".format(post_1.id))
        keyboard.add(button_del)
        posts_format = '\nОбъявление: {0}, \nЦена: {1},\nРод деятельности: {2}, \nАвтор: {3}, \nНомер телефона: {4}, \nГород: {5}'.format(post_1.text_post[:3072], post_1.price, post_1.field_of_activity, post_1.user.name, post_1.user.phone, post_1.user.city)
        bot.send_message(message.chat.id, posts_format[:4095],reply_markup=keyboard)
        print('-----------------------------------------------------------------------')


        @bot.callback_query_handler(func=lambda call: (call.data[0:8] == "del_post" and post_id_find(call.message, call.data.split(' ')[1]))) # возможность удалять объявления самим поьзователем, доступно при выполнении команды /my_posts 
        def process_del_post(call): 
            pd = call.data.split(' ')
            print(pd)
            session.query(Post).filter_by(id=int(pd[1])).delete()
            session.commit()
            message_del(call.message.chat.id)
            

def message_del(tid): # tid - можно как сокращенный telegram_id
    bot.send_message(tid, POST_IS_DEAD)

def post_id_find(message, pid):
    user = find_user(message.chat.id)
    find_poster =session.query(Post).filter(Post.id == int(pid)). one_or_none() 
    return find_poster
        
@bot.callback_query_handler(func=lambda call: (call.data == "del_all" or call.data == "del_cancel"))
def del_all_data(call):
    if call.data == "del_cancel":
        bot.send_message(call.message.chat.id, DEL_ABBORT)
        return
    del_query(call.message)

def del_query(message):
    del_user = find_user(message.chat.id)
    session.query(User).filter(User.telegram_id == del_user.telegram_id).delete()
    session.commit()
    message_del_all(message)


@bot.message_handler(commands=['del_all']) # Команда, позволяющая удалить аккаунт самим пользователем
def shur_del(message):
    if find_user(message.chat.id) is None: 
        bot.send_message(message.chat.id, YOU_CANT_DEL_ACC)
        return
    keyboard = types.InlineKeyboardMarkup()
    button_yes = types.InlineKeyboardButton(YES_MES, callback_data="del_all")
    button_no = types.InlineKeyboardButton(NO_MES, callback_data="del_cancel")
    keyboard.add(button_yes, button_no)
    bot.send_message(message.chat.id, ARE_YOU_SHURE_DEL_ACC, reply_markup=keyboard)


def message_del_all(message):
    bot.send_message(message.chat.id, DEL_ACC_MES)



@bot.message_handler(commands=['upd_location']) # команда позволяет обновлять геоданные пользвователем для более эффективного поиска интересующих объявлений    
def updater_loc(message):
    if find_user(message.chat.id) is None:
       bot.send_message(message.chat.id, CANT_UPD_LOC)
       return
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
    button_location = types.KeyboardButton(text=REG_SEND_LOCATION_MESSAGE, request_location=True)
    keyboard.add(button_location)
    bot.send_message(message.chat.id, BUT_GEOPOS_MES, reply_markup=keyboard)
    bot.register_next_step_handler(message, new_loc)
    
    
@bot.message_handler(content_types=["location"])    
def new_loc(message):
    new_lat = message.location.latitude
    new_lon = message.location.longitude
    session.query(User).filter(User.telegram_id == message.chat.id).update({User.location_latitude:new_lat, User.location_longitude:new_lon})
    session.commit()
    message_update_location(message)

def message_update_location(message):
    bot.send_message(message.chat.id, SUC_GEOPOS_UPDATE_MES)


    

            

if __name__ == '__main__':
    bot.polling(none_stop=True)

