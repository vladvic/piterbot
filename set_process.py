from connect_with_sql_structure import Post_MySql
from sqlalchemy_database import Post, Cot_Post
from session_and_engine_for_reposting import z_session
from mysql_engine_and_session import  q_session
from session_sqlalchemy_database import session
import re
from load_user import load_falsh_user


def query_mysql():
    query_db_mysql = q_session.query(Post_MySql).all()
    return query_db_mysql

def query_cot_post():
    query_db_cot_post = session.query(Cot_Post).all()
    return query_db_cot_post

def kill_copy(conteiner_mysql, conteiner_cot_post):
    set_mysql = set([cont.item_id for cont in conteiner_mysql])# используем цикл (for) внутри метода (set) с заранее указанным параметром(item_id) итерационной переменной(cont) 
    set_cot_post = set([cot_posts.id for cot_posts in conteiner_cot_post])
    deposit_post  = set_mysql - set_cot_post
    print(deposit_post)
    return deposit_post

def deposit_iterator():
    conteiner_mysql = query_mysql()
    conteiner_cot_post = query_cot_post()
    iter_deposit = kill_copy(conteiner_mysql, conteiner_cot_post)
    to_add = [i for i in conteiner_mysql if (i.item_id in iter_deposit)]
    for cot in to_add:
        if cot.user is None:
            continue
        cot_insub_item_text=re.sub('<p>|</p>', "", cot.item_text)
        alter_post = Cot_Post(id=cot.item_id,  item_title=cot.item_title, item_text=cot_insub_item_text)
        z_session.add(alter_post)
        z_session.commit()
        f_user = load_falsh_user(cot.user)
        transport_post = Post(field_of_activity=alter_post.item_title, field_of_activity_lower=alter_post.item_title, text_post=alter_post.item_text, text_post_lower=alter_post.item_text, user=f_user)
        session.add(transport_post)
        session.commit()
    
deposit_iterator()




