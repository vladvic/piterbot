# данный модуль при F5 позволяет запускать бота. При использовании flask ,большое внимание уделять threaded=False - многопоточность и ассихронность.  
import flask
from telebot import types
from config import *
from bot_handlers import bot
import os

server = flask.Flask(__name__)
bot.delete_webhook()
bot.set_webhook(url="https://peterbot.rf70.ru/"+TOKEN, certificate=open('peterbot.pem'))


@server.route('/' + TOKEN, methods=['POST'])
def get_message():
    bot.process_new_updates([types.Update.de_json(flask.request.stream.read().decode("utf-8"))])
    return "!", 200


@server.route('/', methods=["GET"])
def index():
    return "Hello from Heroku!", 200


if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('PORT', 5000)), threaded=False)
