# Данный модуль предназначен для реализации возможности поиска (/search) пользователм сторонних объявлений. Содержит соответсвущий класс Post_search_process 

from messages import *
from telebot import types
from bot import bot
from sqlalchemy_database import Post, User
from session_sqlalchemy_database import session
from sqlalchemy import and_, or_ # позволяет использовать данные функции для обеспечения выбора запросов
import re



processes =  dict()

class Post_search_process:
    def __init__(self, telegramId, finishedCallback):
        global processes
        self._telegram_id = telegramId
        self._callback = finishedCallback
        self._text_post_search = ''
        self._price_post_search = ''
        self._field_of_activity_search = ''
        self.user_real=session.query(User).filter(User.telegram_id == telegramId).one()
        

        processes[telegramId] = self;


    def post_search_start(self, message):
        bot.send_message(self._telegram_id, POST_FIND_START_MESSAGE)
        self.show_text_post_search(message)


    def show_text_post_search(self, message):
        bot.send_message(self._telegram_id, TEXT_POST_FIND_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_text_post_search(message))


    def get_text_post_search(self, message):
        self._text_post_search = message.text
        if self._text_post_search[0]=='/':
            self.show_text_post_search(message)
            return
        self.show_price_post_search(message)


    def show_price_post_search(self, message):
        bot.send_message(self._telegram_id, PRICE_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_price_post_search(message))
        

    def get_price_post_search(self, message): # предусмотрен случай, если пользователь вводит цену исключитлеьно ткстовым собщениием без исопльзования цифровых выражений. 
        self._price_post_search = re.sub('[\D]+', "", message.text) 
        if self._price_post_search  == '':
            if message.text[0]=='/':
                 self.show_price_post_search(message)
                 return
            self._price_post_search = '1000000000'
        self.show_field_of_activity_search(message)
        


    def show_field_of_activity_search(self, message):
        bot.send_message(self._telegram_id, POST_FIELD_OF_ACTIVITY_MESSAGE)
        bot.register_next_step_handler(message, lambda message: self.get_field_of_activity_search(message))


    def get_field_of_activity_search(self, message):
        self._field_of_activity_search = message.text
        if self._field_of_activity_search[0]=='/':
            self.show_field_of_activity_search(message)
            return
        
        bot.send_message(self._telegram_id, FIND_PROCESS_MESSAGE)
        if self._callback is not None:
            self._callback(self)
        self.search_process_post(message)    


    def search_process_post(self, message): #попроцедура запроса в объект Post и объект User
        self.post_search=session.query(Post).join(User, User.id == Post.users_id).filter(and_(Post.price <= int(self._price_post_search), or_ (Post.text_post_lower.like("%{}%".format(self._text_post_search.lower())), Post.field_of_activity_lower.like("%{}%".format(self._field_of_activity_search.lower()))))).order_by((self.user_real.location_latitude-User.location_latitude)*(self.user_real.location_latitude-User.location_latitude)+(self.user_real.location_longitude-User.location_longitude)*(self.user_real.location_longitude-User.location_longitude)).all()
        self.print_search_process()
        
        

    def print_search_process(self): # процедура выввода результат запроса  
        if not self.post_search:
            bot.send_message(self._telegram_id, WE_NOT_FOUND_POST) 
        for post in self.post_search:
            post_format = '\nОбъявление: {0}, \nЦена: {1},\nРод деятельности: {2}, \nАвтор: {3}, \nНомер телефона: {4}, \nГород: {5}'.format(post.text_post[:3072], post.price, post.field_of_activity, post.user.name, post.user.phone, post.user.city)                                                                    
            bot.send_message(self._telegram_id, post_format[:4095])

        
        


    def text_post_search(self):
        return self._text_post_search

    def price_post_search(self):
        return self._price_post_search

    def field_of_activity_search(self):
        return self._field_of_activity_search

    def telegram_id(self):
        return self._telegram_id




        

    
        
        
