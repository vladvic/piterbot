from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
from sqlalchemy import Column, Integer, String, ForeignKey, Float, BigInteger, Text
from sqlalchemy.orm import relationship
from sqlalchemy import Sequence
from mysql_engine_and_session import q_session, engine
from sqlalchemy.orm import object_mapper

class User_MySql(Base):
    __tablename__='cot_users'

    user_id = Column(Integer, Sequence('cot_users_user_id_seq'), primary_key=True)
    user_city = Column(Text, ForeignKey('cot_ls_cities.city_id'))
    user_email = Column(Text)
    posts = relationship ("Post_MySql", backref="user")
   
    def __repr__(self):
        return "<User_MySql(user_city='%s', user_country='%s')>" % (self.user_city, self.user_country)


class City_Country(Base):
    __tablename__='cot_ls_cities'
    
    city_id = Column(Integer, Sequence('cot_ls_cities_city_id_seq'), primary_key=True)
    city_name = Column(Text)
    city_country = Column(Text)
    users = relationship ("User_MySql", backref="ci_co")

    def __repr__(self):
        return "<City_Country(city_name='%s', city_country='%s')>" % (self.city_name, self.city_country)


class Post_MySql(Base):
    __tablename__ = 'cot_projects'

    item_id = Column(Integer, Sequence('cot_projects_item_id_seq'), primary_key=True)
    item_title = Column(Text)
    item_text = Column(Text)
    item_userid = Column(Integer, ForeignKey('cot_users.user_id'))

    def __repr__(self):
        return "<Post_MySql(item_title='%s', item_text='%s')>" % (self.item_title, self.item_text)

if __name__ =='__main__':
    post_mysql = Post_MySql(item_title='разноработа', item_text='треуется разнорабочий')
    my_post_from_sql = q_session.query(Post_MySql).filter_by(item_text='').first()
