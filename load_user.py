from session_sqlalchemy_database import session
from sqlalchemy_database import User

def load_falsh_user(data):
    my_user = session.query(User).filter_by(email=data.user_email).first()
    if my_user is None:
        city_name = ''
        country_code = ''
        if data.ci_co is not None:
            city_name = data.ci_co.city_name
            country_code = data.ci_co.city_country
        my_user = User(phone='96666666666', name='На сайте', email=data.user_email, country=country_code, city=city_name, location_latitude = 0,  location_longitude = 1)
        session.add(my_user)
        session.commit()
    return my_user

